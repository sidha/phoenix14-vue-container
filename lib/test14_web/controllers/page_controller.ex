defmodule Test14Web.PageController do
  use Test14Web, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
