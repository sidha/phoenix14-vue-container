defmodule Test14.Repo do
  use Ecto.Repo,
    otp_app: :test14,
    adapter: Ecto.Adapters.Postgres
end
